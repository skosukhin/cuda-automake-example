#ifndef SAXPY_CUDA_H
#define SAXPY_CUDA_H

#include "saxpy.h"

void saxpy_cuda(int n, float a, float *x, float *y, saxpy_ptr fallback);

#endif // SAXPY_CUDA_H

