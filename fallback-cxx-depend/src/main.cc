#include <iostream>
#include <algorithm>
#include <cmath>

#include "saxpy.h"

int main(int argc, char** argv)
{
  const int N = 1 << 20;

  float *x = new float[N];
  float *y = new float[N];

  for (int i = 0; i < N; i++)
  {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  saxpy(N, 2.0f, x, y);

  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = std::max(maxError, std::abs(y[i] - 4.0f));

  std::cout << "Max error: " << maxError << std::endl;

  delete [] x;
  delete [] y;

  return 0;
}

