#include <stdio.h>

#include "saxpy.h"
#include "saxpy_cuda.h"

__global__
void saxpy_cuda_kernel(int n, float a, float *x, float *y)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n)
  {
    y[i] = a * x[i] + y[i];
  }
}

#define CUDART_CHECK( fn ) do { \
 (status) = (fn); \
 if ( cudaSuccess != (status) ) { \
 fprintf( stderr, "CUDA Runtime Failure (line %d of file %s):\n\t" \
 "%s returned 0x%x (%s)\n", \
 __LINE__, __FILE__, #fn, status, cudaGetErrorString(status) ); \
 goto Error; \
 } \
 } while (0);


void saxpy_cuda(int n, float a, float *x, float *y, saxpy_ptr fallback)
{
  cudaError_t status;
  float *d_x, *d_y;

  CUDART_CHECK( cudaMalloc(&d_x, n * sizeof(float)) );
  CUDART_CHECK( cudaMalloc(&d_y, n * sizeof(float)) );
  CUDART_CHECK( cudaMemcpy(d_x, x, n * sizeof(float), cudaMemcpyHostToDevice) );
  CUDART_CHECK( cudaMemcpy(d_y, y, n * sizeof(float), cudaMemcpyHostToDevice) );

  saxpy_cuda_kernel<<<(n + 255) / 256, 256>>>(n, a, d_x, d_y);

  CUDART_CHECK( cudaPeekAtLastError() );
  CUDART_CHECK( cudaMemcpy(y, d_y, n * sizeof(float), cudaMemcpyDeviceToHost) );

Error:
  cudaFree(d_x);
  cudaFree(d_y);
  if (cudaPeekAtLastError() != cudaSuccess)
  {
    fprintf( stderr, "Running CPU fallback...\n" );
    return fallback(n, a, x, y);
  }
}

