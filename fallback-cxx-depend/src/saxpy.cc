#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "saxpy.h"
#ifdef ENABLE_CUDA
#  include "saxpy_cuda.h"
#endif

static void saxpy_cpu_kernel(int n, float a, float *x, float *y)
{
  for (int i = 0; i < n; ++i)
  {
    y[i] = a * x[i] + y[i];
  }
}

void saxpy(int n, float a, float *x, float *y)
{
#ifdef ENABLE_CUDA
  saxpy_cuda(n, a, x, y, saxpy_cpu_kernel);
#else
  saxpy_cpu_kernel(n, a, x, y);
#endif
}

