#ifndef SAXPY_H
#define SAXPY_H

typedef void (*saxpy_ptr)(int n, float a, float *x, float *y);

void saxpy(int n, float a, float *x, float *y);

#endif // SAXPY_H

