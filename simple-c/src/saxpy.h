#ifndef SAXPY_H
#define SAXPY_H

#ifdef __cplusplus
extern "C"
#endif
void saxpy(int n, float a, float *x, float *y);

#endif // SAXPY_H

