This project provides several examples of how to introduce basic CUDA support in
an
[Autotools](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html)-based
project:
1. [simple-c](./simple-c) &mdash; basic example of a C project;
2. [simple-cxx](./simple-cxx) &mdash; basic example of a C++ project;
3. [simple-cxx-depend](./simple-cxx-depend) &mdash; same as `simple-cxx` with
[automatic dependency tracking](https://www.gnu.org/software/automake/manual/html_node/Dependency-Tracking.html)
for CUDA source files;
4. [fallback-cxx-depend](./fallback-cxx-depend) &mdash; a small project with
optional CUDA support (runs a CPU fallback function when CUDA is either disabled
at build time or the GPU code fails at run time);
5. [separate-cxx-depend](./separate-cxx-depend) &mdash; an example of a project
that requires
[separate compilation and linking of CUDA C++ device code](https://developer.nvidia.com/blog/separate-compilation-linking-cuda-device-code/).

The [m4](./m4) directory contains common Autoconf macros enabling CUDA support.

Use the
[autoreconf](https://gitlab.dkrz.de/skosukhin/cuda-automake-example/-/tree/autoreconf)
branch if you want to check the examples but experience diffuculties when
generating the `configure` scripts.
